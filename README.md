# My Personal Vim config

-----

* YouCompleteMe requires some dependencies and manual installation (see [instructions](https://github.com/ycm-core/YouCompleteMe#quick-start-installing-all-completers)).
* jedi-vim requires manual installation and python3 compatibility (see [instructions](https://github.com/davidhalter/jedi-vim#installation)).
* Remove line 275 in `ayu-theme/colors/ayu.vim` file.

